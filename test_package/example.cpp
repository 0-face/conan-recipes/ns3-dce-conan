#include "ns3/core-module.h"
#include "ns3/dce-module.h"

#include <iostream>

using namespace ns3;
using namespace std;

class DceExample{
public:
    void run(int argc, char ** argv){
        if(!parseArgs(argc, argv)){
            return;
        }

        setup();
        runSimulation();
    }

    bool parseArgs(int argc, char ** argv){
        CommandLine cmd;
        cmd.AddValue("executable",
                     "Path to the program to be executed",
                     this->executable);

        cmd.AddValue("sleep",
                     "Seconds to sleep the executed program",
                     this->sleepSeconds);

        cmd.AddValue("duration",
                     "Total simulation time (in seconds)",
                     this->durationInSeconds);

        cmd.Parse (argc, argv);

        if(this->executable.empty()){
            cerr << "Missing executable path" << endl;
            return false;
        }

        return true;
    }

    void setup(){
        cout << "Configuring nodes" << endl;

        nodes.Create(1);
        dceManager.Install(nodes);

        cout << "Configuring application: "
             << "'" << this->executable << " " << this->sleepSeconds << "'"
             << endl;

        appHelper.SetStackSize(1<<20);
        appHelper.SetBinary(this->executable);
        appHelper.ResetArguments();
        appHelper.AddArgument(std::to_string(this->sleepSeconds));

        apps = appHelper.Install(nodes.Get(0));
        apps.Start(Seconds(0.0));
    }

    void runSimulation(){
        cout << "Starting simulation" << endl;

        Simulator::Stop (Seconds(this->durationInSeconds));
        Simulator::Run ();
        Simulator::Destroy ();

        cout << "Finished simulation" << endl;
    }

private: //options
    double durationInSeconds = 30.0;
    int sleepSeconds = 5;
    std::string executable;

private: //Dce variables
    NodeContainer nodes;
    DceManagerHelper dceManager;
    DceApplicationHelper appHelper;
    ApplicationContainer apps;
};

int main (int argc, char ** argv){
    cout << "*************** start dce example ********************" << endl;

    DceExample example;
    example.run(argc, argv);

    std::cout << "*************** finish example *******************" << std::endl;

    return 0;
}
