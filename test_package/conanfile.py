#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
import os
from os import path

class TestRecipe(ConanFile):
    settings = "os", "compiler", "arch"
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.1.1@noface/stable"
    )

    generators = "Waf"
    exports = "*.cpp", "wscript"

    def imports(self):
        self.copy("*.dll"   , src="bin", dst="bin")
        self.copy("*.dylib*", src="lib", dst="bin")

    def build(self):
        cmd = 'waf -v configure build -o "{}"'.format(path.join(self.build_folder, 'build'))

        self.output.info("Running cmd '{}' in '{}'".format(cmd, self.source_folder))
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        if tools.cross_building(self.settings):
            self.output.info("Cross building - skip run test")
            return

        cwd         = path.join(self.build_folder, 'build')
        example_cmd = './example --executable=sleep'

        env = {}

        # DCE_PATH env var should point to where the binaries that will
        # be executed by the DCE script will be
        env['DCE_PATH'] = cwd

        # We need also to add the DCE lib path to the LD_LIBRARY_PATH env var
        # but the 'ns3-dce' recipe already sets it (using conan env_info variable)
        # env['LD_LIBRARY_PATH'] = ':'.join(self.deps_cpp_info["ns3-dce"].lib_paths)

        self.output.info("running [cwd= {}]:\n\t{}".format(cwd, example_cmd))
        with tools.environment_append(env):
            self.run(example_cmd, cwd=cwd)
